import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityComponent } from './city/city.component';

import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  /* { path: 'customers', component: CustomersComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent }, */
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent },  
  { path: 'welcome', component: WelcomeComponent },
  { path: 'city', component: CityComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
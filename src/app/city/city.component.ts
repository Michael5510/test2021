import { PredictService } from './../predict.service';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/waether';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  cities = [{name:'London', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Paris', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Tel Aviv', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Jerusalem', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Berlin', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Rome', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Dubai', temperature:null, humidity:null, prediction:null, saved: null},
    {name:'Athens', temperature:null, humidity:null, prediction:null, saved: null},
  ];
  constructor(public authService:AuthService, private weatherService:WeatherService,
    private firestore: AngularFirestore,
     private route:ActivatedRoute, public predictService:PredictService) { }

  getDataForCity(city){
    this.weatherService.searchWeatherData(city.name).subscribe((data) => {
      city.temperature = data.temperature; 
      city.image = data.image;
      city.humidity = data.humidity;
    })
  }

  predictCity(city){
    this.predictService.predict(city).subscribe(
      result => {
        if(result.result > 50){
          city.prediction = 'Rain';
        } else {
          city.prediction = 'will not rain'
        }
    });  
  }
  
  saveToFirestore(city) {
    city.saved = true;
    this.firestore.doc('data/cities').set({
      cities: this.cities
    })
  }

  getFromFirestore() {
    this.firestore.doc('data/cities').valueChanges().subscribe((data:any) => {
      if (data) {
        this.cities = data.cities
      }
    })
  }

  ngOnInit(): void {
    this.getFromFirestore()
  }
}



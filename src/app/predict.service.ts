import { Weather } from './interfaces/waether';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://07l0etplb5.execute-api.us-east-1.amazonaws.com/beta"

  predict(city:Weather):Observable<any>{
    let json = 
      {
        "temperature" : city.temperature ,
        "humidity" :city.humidity
      }   
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        return res;       
      })
    );      
  }

  constructor(private http:HttpClient) { }
}

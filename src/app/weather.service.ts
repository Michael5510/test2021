import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, map,  } from 'rxjs/operators';
import { Weather } from './interfaces/waether';
import { WaetherRaw } from './interfaces/waether-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q="; 
  private KEY = "faa55e0c86c7904cdf8f555e150a5ef3"; 
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WaetherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr'); 
  }
  
  private transformWeatherData(data:WaetherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp, 
      lat:data.coord.lat,
      lon:data.coord.lon,
      humidity:data.main.humidity
    } 
  }


}

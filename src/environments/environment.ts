// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    firebaseConfig: {
    apiKey: "AIzaSyA42eRvBGekQglmAq6kfW-XIi7LGJr4bi4",
    authDomain: "test2-michael.firebaseapp.com",
    projectId: "test2-michael",
    storageBucket: "test2-michael.appspot.com",
    messagingSenderId: "842511753074",
    appId: "1:842511753074:web:47cc028da0cc0dd53107eb"
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
